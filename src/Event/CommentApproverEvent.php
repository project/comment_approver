<?php

namespace Drupal\comment_approver\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\comment\CommentInterface;

/**
 * Event that is fired when comment approver approves / blocks a comment.
 */
class CommentApproverEvent extends Event {

  const COMMENT_APPROVED = 'comment_approved';
  const COMMENT_BLOCKED = 'comment_blocked';

  /**
   * The Comment object.
   *
   * @var \Drupal\comment\CommentInterface
   */
  public $comment;

  /**
   * Constructs the object.
   *
   * @param \Drupal\comment\CommentInterface $comment
   *   The Comment object which has been changed by comment approver.
   */
  public function __construct(CommentInterface $comment) {
    $this->comment = $comment;
  }

}
