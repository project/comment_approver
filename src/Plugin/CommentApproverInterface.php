<?php

namespace Drupal\comment_approver\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface for Comment approver plugins.
 */
interface CommentApproverInterface extends ConfigurableInterface, DependentPluginInterface, PluginInspectionInterface {

  /**
   * Return the name of the plugin.
   *
   * @return string
   *   User readable name of the plugin.
   */
  public function getLabel();

  /**
   * Returns the description of Comment Approver.
   *
   * @return string
   *   User readable description of the plugin.
   */
  public function getDescription();

  /**
   * Returns True if comment is ok.
   *
   * @return bool
   *   Should return FALSE if comment does not pass the plugin logic.
   */
  public function isCommentFine($comment);

  /**
   * Returns settings form structure or False if no settings form is needed.
   *
   * @return array|bool
   *   Returns settings form structure or False if no settings form is needed.
   */
  public function settingsForm();

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function settingsFormValidate(array &$form, FormStateInterface $form_state);

  /**
   * Form submit handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function settingsFormSubmit(array &$form, FormStateInterface $form_state);

}
